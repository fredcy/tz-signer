import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import css from 'rollup-plugin-css-only';
import replace from '@rollup/plugin-replace';
import nodePolyfills from 'rollup-plugin-node-polyfills';
import nodeGlobals from 'rollup-plugin-node-globals';


const production = !process.env.ROLLUP_WATCH;

function serve() {
    let server;

    function toExit() {
	if (server) server.kill(0);
    }

    return {
	writeBundle() {
	    if (server) return;
	    server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
		stdio: ['ignore', 'inherit', 'inherit'],
		shell: true
	    });

	    process.on('SIGTERM', toExit);
	    process.on('exit', toExit);
	}
    };
}

export default {
    input: 'src/main.ts',

    output: {
	sourcemap: true,
	format: 'iife',
	name: 'app',
	file: 'public/build/bundle.js',
	globals: {
	    '@airgap/beacon-sdk': 'beaconSdk',
	},
    },

    // This context prevents errors like "`this` has been rewritten to `undefined`"
    context: "window",

    plugins: [
	// Set globally available values for static configuration.
	replace({
	    BOT_HOST: JSON.stringify((production) ? "https://bot.ostez.com" : "http://localhost:4321"),
	    include: "src/**/*",
	}),

	// Monkey-patch Beacon code. Without this I get a rollup
	// error about "Cannot call a namespace ('qrcode')", and the call to
	// requestPermissions() hangs at app runtime.
	replace({
	    "qr = qrcode(": "qr = qrcode.default(",
	    delimiters: ["", ""],
	    include: "node_modules/@airgap/beacon-sdk/**",
	}),

	// The commonjs rewrite needs to happen early in the plugin flow, lest
	// `require()` calls remain in the imported code too long
	commonjs(),

	// Bring in definitions of node.js stuff like 'Buffer'
	nodeGlobals(),
	nodePolyfills(),

	svelte({
	    preprocess: sveltePreprocess({ sourceMap: !production }),
	    compilerOptions: {
		// enable run-time checks when not in production
		dev: !production
	    }
	}),

	// we'll extract any component CSS out into
	// a separate file - better for performance
	css({ output: 'bundle.css' }),

	// If you have external dependencies installed from npm, you'll most
	// likely need these plugins. In some cases you'll need additional
	// configuration - consult the documentation for details:
	// https://github.com/rollup/plugins/tree/master/packages/commonjs
	resolve({
	    browser: true,
	    dedupe: ['svelte']
	}),

	typescript({
	    sourceMap: !production,
	    inlineSources: !production
	}),

	// In dev mode, call `npm run start` once
	// the bundle has been generated
	!production && serve(),

	// Watch the `public` directory and refresh the
	// browser on changes when not in production
	!production && livereload('public'),

	// If we're building for production (npm run build
	// instead of npm run dev), minify
	production && terser()
    ],

    watch: {
	clearScreen: false
    },

    onwarn(warning, warn) {
	// Ignore some "circular dependency cases,
	// per https://github.com/rollup/rollup/issues/2271#issuecomment-455129819
	const ignoredCircular = [
	    "rollup-plugin-node-global",
	    "readable-stream",
	    "@airgap/beacon-sdk",
	];
	if (warning.code === 'CIRCULAR_DEPENDENCY' &&
	    ignoredCircular.some(d => warning.importer.includes(d))) {
	    return
	}
	warn(warning)
    },
};
