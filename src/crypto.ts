import { Buffer } from "buffer";


export function msgToBytes(msg: string) : string {
    const input = Buffer.from(msg);
    const prefix = Buffer.from("0501", "hex");
    const len_bytes = Buffer.from(msg.length.toString(16).padStart(8, '0'), "hex");
    const msgB = Buffer.concat([prefix, len_bytes, input], prefix.length + len_bytes.length + input.length);
    return msgB.toString("hex");
}
