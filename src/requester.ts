export interface TokenInfo {
    message: string,
    network: string,
}

export interface Notification {
    token: string,
    message: string,
    network: string,
    publicKey: string,
    signature: string,
}


export async function getTokenInfo(botHost: string, token: string) : Promise<TokenInfo> {
    const tokenRequestUrl = `${botHost}/api/v1/token/${token}`;

    try {
        const tokenInfo = await fetch(tokenRequestUrl)
            .then(async function (response) {
                //console.log("response", response)
                if (response.ok) {
                    return response.json()
                } else {
		    return Promise.reject(Error(`${response.status} - ${response.statusText}`))
		}
	    })
        return tokenInfo
    } catch (err) {
	throw new Error(`fetch of ${tokenRequestUrl} failed: ${err.message}`)
    }
}


export async function notify(botHost: string, notification: Notification) : Promise<any> {
    // Notify the application of the signed message via its webhook.
    const botUrl = `${botHost}/api/v1/register`;
    const body = JSON.stringify(notification);

    const request = new Request(botUrl, {
        headers: {
             "content-type": "application/json"
         },
         body: body,
         method: "POST"
     });

     try {
         const response : any = await Promise.race([
             fetch(request),
             new Promise((_, reject) => setTimeout(
                 () => reject(new Error('Timeout')),
                 10000
             )),
         ]);
         return response.json()
     } catch (error) {
         console.log("timed fetch error:", error);
         if (error.message.startsWith("NetworkError")) {
             throw new Error("Network error when notifying bot. Is the bot server running?")
         } else if (error.message === "Timeout") {
             throw new Error("Timed out when notifying the bot")
         } else {
	     throw new Error(error.message)
         }
     }

}
